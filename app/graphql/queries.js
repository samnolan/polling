// eslint-disable
// this is an auto generated file. This will be overwritten

export const getProposal = `query GetProposal($id: ID!) {
  getProposal(id: $id) {
    id
    title
    description
    createdAt
    votes {
      items {
        id
        createdAt
      }
      nextToken
    }
  }
}
`;
export const listProposals = `query ListProposals(
  $filter: ModelProposalFilterInput
  $limit: Int
  $nextToken: String
) {
  listProposals(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      title
      description
      createdAt
      votes {
        nextToken
      }
    }
    nextToken
  }
}
`;
export const getVote = `query GetVote($id: ID!) {
  getVote(id: $id) {
    id
    createdAt
    proposal {
      id
      title
      description
      createdAt
      votes {
        nextToken
      }
    }
  }
}
`;
export const listVotes = `query ListVotes(
  $filter: ModelVoteFilterInput
  $limit: Int
  $nextToken: String
) {
  listVotes(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      createdAt
      proposal {
        id
        title
        description
        createdAt
      }
    }
    nextToken
  }
}
`;
