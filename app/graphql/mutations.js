// eslint-disable
// this is an auto generated file. This will be overwritten

export const createProposal = `mutation CreateProposal($input: CreateProposalInput!) {
  createProposal(input: $input) {
    id
    title
    description
    createdAt
    votes {
      items {
        id
        createdAt
      }
      nextToken
    }
  }
}
`;
export const updateProposal = `mutation UpdateProposal($input: UpdateProposalInput!) {
  updateProposal(input: $input) {
    id
    title
    description
    createdAt
    votes {
      items {
        id
        createdAt
      }
      nextToken
    }
  }
}
`;
export const deleteProposal = `mutation DeleteProposal($input: DeleteProposalInput!) {
  deleteProposal(input: $input) {
    id
    title
    description
    createdAt
    votes {
      items {
        id
        createdAt
      }
      nextToken
    }
  }
}
`;
export const createVote = `mutation CreateVote($input: CreateVoteInput!) {
  createVote(input: $input) {
    id
    createdAt
    proposal {
      id
      title
      description
      createdAt
      votes {
        nextToken
      }
    }
  }
}
`;
export const updateVote = `mutation UpdateVote($input: UpdateVoteInput!) {
  updateVote(input: $input) {
    id
    createdAt
    proposal {
      id
      title
      description
      createdAt
      votes {
        nextToken
      }
    }
  }
}
`;
export const deleteVote = `mutation DeleteVote($input: DeleteVoteInput!) {
  deleteVote(input: $input) {
    id
    createdAt
    proposal {
      id
      title
      description
      createdAt
      votes {
        nextToken
      }
    }
  }
}
`;
