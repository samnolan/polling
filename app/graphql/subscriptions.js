// eslint-disable
// this is an auto generated file. This will be overwritten

export const onCreateProposal = `subscription OnCreateProposal {
  onCreateProposal {
    id
    title
    description
    createdAt
    votes {
      items {
        id
        createdAt
      }
      nextToken
    }
  }
}
`;
export const onUpdateProposal = `subscription OnUpdateProposal {
  onUpdateProposal {
    id
    title
    description
    createdAt
    votes {
      items {
        id
        createdAt
      }
      nextToken
    }
  }
}
`;
export const onDeleteProposal = `subscription OnDeleteProposal {
  onDeleteProposal {
    id
    title
    description
    createdAt
    votes {
      items {
        id
        createdAt
      }
      nextToken
    }
  }
}
`;
export const onCreateVote = `subscription OnCreateVote {
  onCreateVote {
    id
    createdAt
    proposal {
      id
      title
      description
      createdAt
      votes {
        nextToken
      }
    }
  }
}
`;
export const onUpdateVote = `subscription OnUpdateVote {
  onUpdateVote {
    id
    createdAt
    proposal {
      id
      title
      description
      createdAt
      votes {
        nextToken
      }
    }
  }
}
`;
export const onDeleteVote = `subscription OnDeleteVote {
  onDeleteVote {
    id
    createdAt
    proposal {
      id
      title
      description
      createdAt
      votes {
        nextToken
      }
    }
  }
}
`;
