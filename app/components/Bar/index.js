import styled from 'styled-components';

const Bar = styled.div`
    padding: 0.25em 0;
    height: 2em;

    width ${props => `${props.width * 100}%`};

    background: hsl(${props => `${props.hue}`}, 80%, 50%);
    display: inline-block;
    vertical-align: middle;

`;

export default Bar;
