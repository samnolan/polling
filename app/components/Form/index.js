/**
 * A link to a certain page, an anchor tag
 */

import styled from 'styled-components';

const Form = styled.form`
  color: #41addd;

  &:hover {
    color: #6cc0e5;
  }
`;

export default Form;
