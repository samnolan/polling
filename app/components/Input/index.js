import styled from 'styled-components';
import React from 'react';

const InnerInput = styled.input`
  outline: none;
  border-bottom: 1px dotted #999;
`;
const Label = styled.label``;

const Input = props => {
  const { label, ...rest } = props;
  return (
    <span>
      {label && <Label>{label}</Label>}
      <br />
      <InnerInput {...rest} />
    </span>
  );
};

export default Input;
