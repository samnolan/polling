/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const CONSTANT_PREFIX = 'poll/Home/';
export const RESPONSE_SUFFIX = '_RESPONSE';
export const ERROR_SUFFIX = '_ERROR';

const createConstantSet = actionType => ({
  start: CONSTANT_PREFIX + actionType,
  response: CONSTANT_PREFIX + actionType + RESPONSE_SUFFIX,
  error: CONSTANT_PREFIX + actionType + ERROR_SUFFIX,
});

export const CHANGE_USERNAME = 'boilerplate/Home/CHANGE_USERNAME';

export const SUBSCRIBE_ADD_PROPOSAL = createConstantSet(
  'SUBSCRIBE_ADD_PROPOSAL',
);

export const SUBSCRIBE_DELETE_PROPOSAL = createConstantSet(
  'SUBSCRIBE_DELETE_PROPOSAL',
);

export const SUBSCRIBE_ADD_VOTE = createConstantSet('SUBSCRIBE_ADD_VOTE');
export const ADD_PROPOSAL = createConstantSet('ADD_PROPOSAL');
export const GET_PROPOSALS = createConstantSet('GET_PROPOSALS');

export const VOTE = createConstantSet('VOTE');

export const REMOVE_PROPOSAL = createConstantSet('REMOVE_PROPOSAL');
