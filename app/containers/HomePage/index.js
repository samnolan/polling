/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import H2 from 'components/H2';
import Timestamp from 'react-timestamp';
import Input from 'components/Input';

import { Doughnut, Line } from 'react-chartjs-2';

// import { Auth } from 'aws-amplify';

import CenteredSection from './CenteredSection';
import {
  addProposal,
  getProposals,
  subscribeAddProposal,
  subscribeDeleteProposal,
  subscribeAddVote,
  vote,
  removeProposal,
} from './actions';
import { makeSelectProposals } from './selectors';
import reducer from './reducer';
import saga from './saga';

import Button from '../../components/Button';
import Bar from '../../components/Bar';

/* eslint-disable react/prefer-stateless-function */
export class HomePage extends React.Component {
  /**
   * when initial state username is not null, submit the form to load repos
   */

  constructor(props) {
    super(props);
    this.state = {
      selected: null,
      proposal: '',
      description: '',
    };
  }

  updateState(event, key) {
    this.setState({ [key]: event.target.value });
  }

  componentDidMount() {
    this.props.getProposals();
    this.props.subscribeAddProposal();
    this.props.subscribeDeleteProposal();
    this.props.subscribeAddVote();
  }

  setSelected = id => {
    if (this.state.selected === id) {
      this.setState({ selected: null });
    } else {
      this.setState({ selected: id });
    }
  };

  render() {
    const { proposals } = this.props;

    let maxCount = 0;
    proposals.forEach(proposal => {
      const temp = proposal.votes.items.length;
      if (temp > maxCount) {
        maxCount = temp;
      }
    });
    proposals.sort((a, b) => b.votes.items.length - a.votes.items.length);

    const propTitles = proposals.map(proposal => String(proposal.title));
    const propVotes = proposals.map(proposal => proposal.votes.items.length);

    const data = {
      labels: propTitles,
      datasets: [
        {
          data: propVotes,
          backgroundColor: proposals.map(
            proposal => `hsl(${parseInt(proposal.id, 16) % 360},100%,50%)`,
          ),
        },
      ],
    };

    let maxTime = null;
    let minTime = null;
    const proposalDataSets = proposals.map(proposal => ({
      label: proposal.title,
      lineTension: 0,
      data: proposal.votes.items
        .sort(
          (a, b) =>
            new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime(),
        )
        .map((voteItem, index) => {
          const time = new Date(voteItem.createdAt);
          if (maxTime == null || maxTime < time) {
            maxTime = time;
          }
          if (minTime == null || minTime > time) {
            minTime = time;
          }
          return {
            t: time,
            y: index + 1,
          };
        }),
      fill: false,
      borderColor: `hsl(${parseInt(proposal.id, 16) % 360},100%,50%)`,
      backgroundColor: `hsl(${parseInt(proposal.id, 16) % 360},100%,50%)`,
    }));

    const lineChart = {
      datasets: proposalDataSets,
    };

    const lineOption = {
      scales: {
        yAxes: [
          {
            scaleLabel: {
              display: true,
              labelString: 'Number of Vote',
            },
          },
        ],
        xAxes: [
          {
            type: 'time',
            scaleLabel: {
              display: true,
              labelString: 'Date',
            },
            time: {
              min: minTime,
              max: maxTime,
            },
          },
        ],
      },
      showLine: true,
    };

    // Auth.currentAuthenticatedUser({
    // bypassCache: false,
    // })
    // .then(user => console.log(user))
    // .catch(err => console.log(err));

    return (
      <article>
        <Helmet>
          <title>Home Page</title>
          <meta name="description" content="Polling Application homepage" />
        </Helmet>
        <div>
          {proposals.length > 0 && (
            <div>
              <Doughnut data={data} />
              <br />
              <Line data={lineChart} options={lineOption} />
            </div>
          )}
          <CenteredSection>
            <H2>Please submit your proposals here</H2>
            <Input
              label="Title"
              onChange={e => this.updateState(e, 'proposal')}
              value={this.state.proposal}
            />
            <br />
            <Input
              label="Description"
              onChange={e => this.updateState(e, 'description')}
              value={this.state.description}
            />
            <br />
            <Input
              type="button"
              value="Submit"
              onClick={() =>
                this.props.addProposal({
                  title: this.state.proposal,
                  description: this.state.description,
                })
              }
            />
          </CenteredSection>
        </div>
        {proposals.map(proposal => (
          /* eslint-disable */
          <div
            onClick={() => this.setSelected(proposal.id)}
            key={proposal.id}
            role="button"
          >
            <div
              style={{
                display: 'inline-block',
                width: '30%',
                textAlign: 'right',
                verticalAlign: 'middle',
              }}
            >
              <span style={{ paddingRight: '1em' }}>{proposal.title}</span>
            </div>
            <div style={{ display: 'inline-block', width: '40%' }}>
              <Bar
                width={(proposal.votes.items.length / maxCount) * 0.8}
                hue={parseInt(proposal.id, 16) % 360}
              />
              <span style={{ paddingLeft: '1em' }}>
                {proposal.votes.items ? proposal.votes.items.length : 0}
              </span>
            </div>
            <div style={{ display: 'inline-block' }}>
              <Button
                onClick={e => {
                  e.stopPropagation();
                  this.props.vote({
                    voteProposalId: proposal.id,
                  });
                }}
              >
                Yes
              </Button>
              <Button onClick={() => this.props.removeProposal(proposal)}>
                Delete
              </Button>
            </div>
            <div style={{ clear: 'both' }} />
            <div
              style={{
                display: this.state.selected === proposal.id ? 'block' : 'none',
                paddingLeft: '30%',
                wordWrap: 'break-word',
                maxWidth: '94%',
              }}
            >
              {proposal.description}
              <br />
              Created <Timestamp time={proposal.createdAt} />
            </div>
          </div>
        ))}
      </article>
    );
  }
}

HomePage.propTypes = {
  proposals: PropTypes.any,
  getProposals: PropTypes.func,
  subscribeAddProposal: PropTypes.func,
  subscribeDeleteProposal: PropTypes.func,
  subscribeAddVote: PropTypes.func,
  addProposal: PropTypes.func,
  vote: PropTypes.func,
  removeProposal: PropTypes.func,
};

export const mapDispatchToProps = {
  getProposals: getProposals.start,
  addProposal: addProposal.start,
  subscribeAddProposal: subscribeAddProposal.start,
  subscribeDeleteProposal: subscribeDeleteProposal.start,
  subscribeAddVote: subscribeAddVote.start,
  vote: vote.start,
  removeProposal: removeProposal.start,
};

const mapStateToProps = createStructuredSelector({
  proposals: makeSelectProposals(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
