import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.get('home', initialState);

const makeSelectProposals = () =>
  createSelector(selectHome, homeState => homeState.get('proposals'));

export { selectHome, makeSelectProposals };
