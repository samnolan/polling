/**
 * Gets poll data
 */

import { take, call, put, takeLatest, takeEvery } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import { API, graphqlOperation } from 'aws-amplify';
import * as queries from 'graphql/queries';
import * as subscriptions from 'graphql/subscriptions';
import * as mutations from 'graphql/mutations';
import {
  GET_PROPOSALS,
  ADD_PROPOSAL,
  SUBSCRIBE_ADD_PROPOSAL,
  SUBSCRIBE_ADD_VOTE,
  VOTE,
  REMOVE_PROPOSAL,
  SUBSCRIBE_DELETE_PROPOSAL,
} from './constants';
import {
  getProposals,
  addProposal,
  subscribeAddProposal,
  subscribeAddVote,
  vote,
  removeProposal,
  subscribeDeleteProposal,
} from './actions';

/**
 * Proposal get
 */

const createSagaFromAsync = (actionGen, asyncfunc) =>
  function* generator(action) {
    try {
      const response = yield call(asyncfunc, action);
      yield put(actionGen.response(response));
    } catch (err) {
      yield put(actionGen.error(err));
    }
  };

function deleteProposalChannel() {
  return eventChannel(emit => {
    const subscription = API.graphql(
      graphqlOperation(subscriptions.onDeleteProposal),
    ).subscribe({
      next: todoData => emit(todoData),
      error: err => emit(new Error(err)),
    });

    function unsubscribe() {
      subscription.unsubscribe();
    }

    return unsubscribe;
  });
}

function createChangeChannel() {
  return eventChannel(emit => {
    const subscription = API.graphql(
      graphqlOperation(subscriptions.onCreateProposal),
    ).subscribe({
      next: todoData => emit(todoData),
      error: err => emit(new Error(err)),
    });

    function unsubscribe() {
      subscription.unsubscribe();
    }

    return unsubscribe;
  });
}

function createVoteChannel() {
  return eventChannel(emit => {
    const subscription = API.graphql(
      graphqlOperation(subscriptions.onCreateVote),
    ).subscribe({
      next: todoData => emit(todoData),
      error: err => emit(new Error(err)),
    });

    function unsubscribe() {
      subscription.unsubscribe();
    }

    return unsubscribe;
  });
}

function* subscribeAddProposalSaga() {
  try {
    const subscriptionChannel = yield call(createChangeChannel);
    while (true) {
      const payload = yield take(subscriptionChannel);
      yield put(subscribeAddProposal.response(payload));
    }
  } catch (err) {
    yield put(subscribeAddProposal.error(err));
  }
}

function* subscribeAddVoteSaga() {
  try {
    const subscriptionChannel = yield call(createVoteChannel);
    while (true) {
      const payload = yield take(subscriptionChannel);
      yield put(subscribeAddVote.response(payload));
    }
  } catch (err) {
    yield put(subscribeAddVote.error(err));
  }
}

function* subscribeRemoveProposalSaga() {
  try {
    const subscriptionChannel = yield call(deleteProposalChannel);
    while (true) {
      const payload = yield take(subscriptionChannel);
      yield put(subscribeDeleteProposal.response(payload));
    }
  } catch (err) {
    yield put(subscribeDeleteProposal.error(err));
  }
}
/* eslint-disable */

const groupBy = function(xs, key) {
  return xs.reduce((rv, x) => {
    (rv[key(x)] = rv[key(x)] || []).push(x);
    return rv;
  }, {});
};

/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(
    GET_PROPOSALS.start,
    createSagaFromAsync(getProposals, async () => {
      let response = await API.graphql(
        graphqlOperation(queries.listProposals, { limit: 1000 }),
      );
      let proposals = [].concat(response.data.listProposals.items);
      while (response.data.listProposals.nextToken) {
        response = await API.graphql(
          graphqlOperation(queries.listProposals, {
            nextToken: response.data.listProposals.nextToken,
            limit: 1000,
          }),
        );
        proposals = proposals.concat(response.data.listProposals.items);
      }

      let voteResponse = await API.graphql(
        graphqlOperation(queries.listVotes, { limit: 1000 }),
      );
      let token = voteResponse.data.listVotes.nextToken;
      let votes = [].concat(voteResponse.data.listVotes.items);
      while (token) {
        voteResponse = await API.graphql(
          graphqlOperation(queries.listVotes, {
            nextToken: token,
            limit: 1000,
          }),
        );
        votes = votes.concat(voteResponse.data.listVotes.items);
        token = voteResponse.data.listVotes.nextToken;
      }

      const groupByProposal = groupBy(votes, item => item.proposal.id);
      response.data.listProposals.items = proposals.map(proposal => {
        if (groupByProposal[proposal.id]) {
          return {
            ...proposal,
            votes: { items: groupByProposal[proposal.id] },
          };
        }
        return {
          ...proposal,
          votes: { items: [] },
        };
      });
      return response;
    }),
  );
  yield takeEvery(
    ADD_PROPOSAL.start,
    createSagaFromAsync(addProposal, action =>
      API.graphql(
        graphqlOperation(mutations.createProposal, {
          input: action.proposal,
        }),
      ),
    ),
  );

  yield takeEvery(SUBSCRIBE_ADD_PROPOSAL.start, subscribeAddProposalSaga);
  yield takeEvery(SUBSCRIBE_ADD_VOTE.start, subscribeAddVoteSaga);
  yield takeEvery(SUBSCRIBE_DELETE_PROPOSAL.start, subscribeRemoveProposalSaga);
  yield takeEvery(
    VOTE.start,
    createSagaFromAsync(vote, action =>
      API.graphql(
        graphqlOperation(mutations.createVote, { input: action.vote }),
      ),
    ),
  );
  yield takeEvery(
    REMOVE_PROPOSAL.start,
    createSagaFromAsync(removeProposal, async action => {
      let promises = action.proposal.votes.items.map(vote => {
        return API.graphql(
          graphqlOperation(mutations.deleteVote, {
            input: { id: vote.id },
          }),
        );
      });
      await Promise.all(promises);
      return await API.graphql(
        graphqlOperation(mutations.deleteProposal, {
          input: { id: action.proposal.id },
        }),
      );
    }),
  );
}
