/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';

import {
  GET_PROPOSALS,
  SUBSCRIBE_ADD_PROPOSAL,
  SUBSCRIBE_ADD_VOTE,
  SUBSCRIBE_DELETE_PROPOSAL,
} from './constants';

// The initial state of the App
export const initialState = fromJS({
  proposals: [],
});

function homeReducer(state = initialState, action) {
  switch (action.type) {
    case GET_PROPOSALS.response:
      return state.set('proposals', action.response.data.listProposals.items);
    case SUBSCRIBE_ADD_PROPOSAL.response:
      return state.update('proposals', proposals =>
        proposals.concat([action.response.value.data.onCreateProposal]),
      );
    case SUBSCRIBE_DELETE_PROPOSAL.response:
      return state.update('proposals', proposals =>
        proposals.filter(
          proposal =>
            proposal.id !== action.response.value.data.onDeleteProposal.id,
        ),
      );
    case SUBSCRIBE_ADD_VOTE.response:
      return state.update('proposals', proposals =>
        proposals.map(item => {
          const vote = action.response.value.data.onCreateVote;
          if (item.id === vote.proposal.id) {
            return {
              ...item,
              votes: {
                items: item.votes.items.concat(vote),
              },
            };
          }
          return item;
        }),
      );
    default:
      return state;
  }
}

export default homeReducer;
