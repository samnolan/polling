/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  SUBSCRIBE_ADD_PROPOSAL,
  SUBSCRIBE_ADD_VOTE,
  ADD_PROPOSAL,
  GET_PROPOSALS,
  VOTE,
  REMOVE_PROPOSAL,
  SUBSCRIBE_DELETE_PROPOSAL,
} from './constants';

const createActionSet = (actionType, original) => ({
  start: (...args) => {
    const action = original(...args);
    action.type = actionType.start;
    return action;
  },
  response: response => ({
    type: actionType.response,
    response,
  }),
  error: error => ({
    type: actionType.error,
    error,
  }),
});

export const addProposal = createActionSet(ADD_PROPOSAL, proposal => ({
  proposal,
}));

export const subscribeAddProposal = createActionSet(
  SUBSCRIBE_ADD_PROPOSAL,
  () => ({
    type: SUBSCRIBE_ADD_PROPOSAL,
  }),
);

export const subscribeDeleteProposal = createActionSet(
  SUBSCRIBE_DELETE_PROPOSAL,
  () => ({
    type: SUBSCRIBE_DELETE_PROPOSAL,
  }),
);

export const subscribeAddVote = createActionSet(SUBSCRIBE_ADD_VOTE, () => ({
  type: SUBSCRIBE_ADD_VOTE,
}));

export const getProposals = createActionSet(GET_PROPOSALS, () => ({}));

export const vote = createActionSet(VOTE, voteData => ({
  vote: voteData,
}));

export const removeProposal = createActionSet(REMOVE_PROPOSAL, proposal => ({
  proposal,
}));
